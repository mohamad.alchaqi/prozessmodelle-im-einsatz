﻿**Mohamad Alchaqi
Softwareengineering
Prof. Dr.-Ing. Ammar Memari**

**Einleitung**

Wenn wir über den Softwareentwicklungsprozess sprechen, geht es um die Implementierung einer Struktur in die Designentwicklung von Software. Es zielt darauf ab, ein Softwareprodukt zu erstellen, das die Ziele der Benutzer erfüllt. Der Entwicklungsprozess für benutzerdefinierte Anwendungen besteht aus einem detaillierten Plan, der die Entwicklung, Wartung, Ersetzung und Verbesserung der Software beschreibt.

Dieser Prozess wird im Volksmund auch als Softwareentwicklungslebenszyklus bezeichnet. Es gibt einige Phasen, die befolgt werden müssen, um die Entwicklung eines benutzerdefinierten Softwareprodukts abzuschließen. Die SDLC- oder Systems Development Life Cycle-Methode unterstützt das Design von Software, die die Geschäftsanforderungen erfüllt. Sie sind – Planen, Definieren von Anforderungen, Architekturdesign, Entwickeln, Testen und Bereitstellen.

Es ist ein iterativer Prozess, der darauf abzielt, eine programmierte Software zu erstellen, die die Geschäftsziele erfüllt. Verschiedene Tools und Programmiersprachen werden verwendet, um eine Softwarelösung und eine mobile App für verschiedene Betriebssysteme zu erstellen. Es ermöglicht Unternehmen, maßgeschneiderte Lösungen zu entwickeln und an Popularität zu gewinnen. Organisationen können ihre Dienste an jedem Ort für jedermann zugänglich machen. Die Forderung nach einem besseren Management der Entwicklungs- und Testphase verstärkte den Zweig des Software-Engineerings, der sich auf eine systematische Herangehensweise an den Prozess der Softwareentwicklung konzentriert.

Mit Softwareentwicklung können Unternehmen ihre Verkaufs- und Kundenerfahrung verbessern. Es ist auch vorteilhaft, Konversion zu liefern, indem der eingehende Verkehr berechnet wird.

**Das Spiralmodell** 

es ist ein Typ des Anwendungsentwicklungsprozesses. Es basiert auf der Prämisse, dass die Softwareentwicklung ein iterativer Zyklus ist, der wiederholt wird, bis die gesetzten Ziele erreicht sind. Es ist in der Lage, mit einer großen Anzahl von Risiken umzugehen, die bei der Entwicklung eines Programms auftreten können.

Es ist eines der wichtigsten Modelle zur Unterstützung des Risikomanagements. Wie der Name schon sagt, erscheint dieses Modell als Spirale, in der die verschiedenen Phasen des Modells in verschiedenen Zyklen verteilt sind. Die Anzahl der Zyklen im Modell ist nicht festgelegt und kann von Projekt zu Projekt variieren.

![](Aspose.Words.e4ac19bc-ca96-443e-988f-2402ad6e865f.001.jpeg)
Bild 1: The diagram shows the different phases of the Spiral Model (1)

Jeder Ebene stellt einen vollständigen Zyklus dar, während dessen die vier Quadranten immer durchlaufen werden und die vier Phasen des Modells darstellen. Die Stufen werden nicht nur einmal, sondern mehrfach spiralförmig ausgeführt. Obwohl diese periodische Iteration das Projekt langsam den gesetzten Zielen näherbringt, wird das Risiko des Scheiterns des Entwicklungsprozesses stark reduziert.

**US-Armee** 

Das Spiralmodell wurde von der US-Armee übernommen, um das SCF-Modernisierungsprogramm zu entwickeln und zu aktualisieren.
Die SCF-Rakete wurde 2003 offiziell gestartet, um Truppen mit Fahrzeugen auszustatten, die in Echtzeit mit einem ungewöhnlich schnellen und flexiblen Netzwerk von Schlachtfeldern verbunden sind.
Das Projekt wurde in vier Entwicklungszyklen von jeweils etwa zwei Jahren aufgeteilt. Spiral 1 sollte 2008 beginnen und Prototypen für den Einsatz und die Bewertung bereitstellen. Nach Fertigstellung von Spirale 1 sollte Spirale 2 im Jahr 2010 beginnen. Die Entwicklung des Endprodukts sollte 2015 geliefert werden.
Im August 2005 gab Boeing den Abschluss des ersten großen Projektabschlusses bekannt.

Im Oktober 2005 empfahl das Pentagon jedoch, das Projekt aufgrund der erheblichen Auswirkungen auf die Kosten durch den Irakkrieg und die Hilfe durch den Hurrikan Katrina zu verschieben.

Das Projekt wurde 2009 nach sich abzeichnenden Budgetkürzungen abgebrochen, ohne den Nutzen des Spiralmodells bei dieser Aufgabenstellung nachweisen zu können.



Vorteilen:

1. **periodische Struktur:** Durch diese Art von Struktur werden die Probleme zwischen dem Design und den technischen Anforderungen des Programms dank regelmäßiger Überprüfungen implizit gelöst.
1. **Risikomanagement:** Risiken werden in jeder Phase des Produkts analysiert, bevor es weitergeht. Dies hilft, potenzielle Risiken zu überwinden oder zu mindern. Alle Mitarbeiter profitieren von der hohen Bedeutung der Risikoanalyse in diesem Modell, was ihren größten Vorteil gegenüber anderen Vorgehensmodellen darstellen kann. Eine regelmäßige Risikobewertung ist wertvoll beim Einsatz neuer technischer Umgebungen, die aufgrund fehlender Erfahrungswerte in der Regel mit dem Potenzial für bestimmte Risiken verbunden sind.
1. **Perfekt für große Projekte:** Es ist besonders beliebt und beliebt bei großen und komplexen Projekten, bei denen die Budgetkontrolle für Kunden und Entwickler Priorität hat. Sie haben die maximale Kontrolle über Kosten, Ressourcen und Qualität eines Softwareprojekts.

Nachteilen:

1. **Teuer:** Es kann sehr kostspielig sein, da es ein hohes Maß an Fachwissen für die Risikoanalyse erfordert. Darüber hinaus dauert die Entwicklung von Projekten lange, was zu erhöhten Gemeinkosten führen kann.
1. **komplette Stille:** Aktives und komplexes vorheriges Projektmanagement ist erforderlich, da jeder Zyklus kontinuierlich und genau kontrolliert und dokumentiert wird. Es ist relativ komplexer als andere Modelle, da es viele Zyklen gibt, die jeweils unterschiedliche Phasen durchlaufen, was den Aufwand des Dokumentationsprozesses erhöht. Kenntnisse in Risikoanalyse und -management sind unerlässlich und oft nicht vorhanden.
1. **Zeitmanagement:** Das Zeitmanagement ist schwierig, da die Anzahl der Zyklen unbekannt ist. Zudem kann sich der Entwicklungsprozess jederzeit verzögern, wenn in einem Zyklus wichtige Entscheidungen anstehen oder bei der Planung der nächsten zusätzlichen Maßnahme ergriffen werden sollen.
1. **viele Schritte** Es ist nicht immer eine gute Idee, viele Schritte in der Softwareentwicklung durchzuführen, da es trotz der Vielseitigkeit des Testens unvollständige Teile der Software bis zum endgültigen System schaffen können.


**Das Wasserfallmodell** 

Es war das erste Prozessmodell, das eingeführt wurde. Es wird auch als linear-sequentielles Lebenszyklusmodell bezeichnet. Es ist sehr einfach zu verstehen und zu verwenden. In einem Wasserfallmodell muss jede Phase abgeschlossen sein, bevor die nächste Phase beginnen kann, und es gibt keine Überschneidungen in den Phasen. Bei diesem Wasserfallmodell überschneiden sich die Phasen nicht.

![](Aspose.Words.e4ac19bc-ca96-443e-988f-2402ad6e865f.002.jpeg)
Bild 2: Die sequenziellen Phasen im Wasserfallmodell (2)

**Cisco Systems, Inc**. 

eist ein amerikanisches multinationales Technologieunternehmen mit Hauptsitz in San Jose, Kalifornien, USA. Cisco entwickelt, produziert und vertreibt Netzwerkhardware, Software, Telekommunikationsgeräte und andere Hightech-Dienste und -Produkte. Cisco wurde über mehrere Tochtergesellschaften wie OpenDNS, WebEx und XPB erworben und ist auf bestimmte Technologiemärkte wie das Internet der Dinge, Internetdomänen- und Energiemanagement spezialisiert.

Im Jahr 2020 stufte das Fortune-Magazin Cisco basierend auf einer Umfrage zur Mitarbeiterzufriedenheit auf Platz 4 seiner jährlichen Liste der 100 besten Arbeitgeber für 2020 ein. Es sucht ständig nach neuen Wegen, schneller und einfacher zu werden. Als Teil seiner digitalen IT-Strategie wollte die Cisco Cloud and Software IT (CSIT)-Organisation eine agilere Entwicklung einführen, um regelmäßige Hauptversionen durch die kontinuierliche Bereitstellung neuer Funktionen zu ersetzen. „Unser Ziel ist es, Releases zu beschleunigen, die Produktivität zu steigern und die Qualität zu verbessern“, sagt Ashish Pandey, technischer Leiter des CSIT-Teams. Obwohl einige kleine Teams agile Techniken eingeführt hatten, war Wasserfall immer noch die Norm für große Teams, die verteilt waren oder an komplexen Projekten arbeiteten. (3)

**Vorteile**

-Die Vorteile der Wasserfallentwicklung bestehen darin, dass sie Abteilungsbildung und Kontrolle ermöglicht. Für jede Entwicklungsphase kann ein Zeitplan mit Fristen festgelegt werden, und ein Produkt kann nacheinander die Phasen des Entwicklungsprozessmodells durchlaufen.
-Die Entwicklung erstreckt sich vom Konzept über Design, Implementierung, Test, Installation, Fehlerbehebung bis hin zu Betrieb und Wartung. Jede Phase der Entwicklung verläuft in strenger Reihenfolge.
-Einfach und leicht zu verstehen und zu verwenden
-Einfache Handhabung aufgrund der Steifigkeit des Modells. Jede Phase hat spezifische Ergebnisse und einen Überprüfungsprozess.
-Phasen werden einzeln bearbeitet und abgeschlossen.
-Gut verstandene Meilensteine.
-Prozess und Ergebnisse sind gut dokumentiert.

**Nachteile**

-Der Nachteil der Wasserfallentwicklung besteht darin, dass sie nicht viel Reflexion oder Überarbeitung zulässt. Sobald sich eine Anwendung in der Testphase befindet, ist es sehr schwierig, zurückzugehen und etwas zu ändern, das in der Konzeptphase nicht gut dokumentiert oder durchdacht war.
-Bis spät im Lebenszyklus wird keine funktionierende Software produziert.
-Hohe Risiken und Ungewissheiten.
-Nicht geeignet für Projekte, bei denen Anforderungen ein mittleres bis hohes Änderungsrisiko aufweisen. Risiko und Ungewissheit sind bei diesem Prozessmodell also hoch.
-Es ist schwierig, den Fortschritt innerhalb von Stufen zu messen.
-Kann geänderte Anforderungen nicht berücksichtigen.
-Die Anpassung des Umfangs während des Lebenszyklus kann ein Projekt beenden.
-Integration erfolgt ganz am Ende als „Big-Bang“, der es nicht erlaubt, technologische oder geschäftliche Engpässe oder Herausforderungen frühzeitig zu erkennen.


Einige linke 

1. <https://www.geeksforgeeks.org/software-engineering-spiral-model/> 
1. <https://www.tutorialspoint.com/sdlc/sdlc_waterfall_model.htm>
1. ` `<https://www.scaledagileframework.com/cisco-case-study/>

